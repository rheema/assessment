﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE
{
    public class Commandparser
    {
        //store the variables and the value access value by way of variable key
        Dictionary<string, string> VariablesToValueDictionary = new Dictionary<string, string>();
        drawingclass Mydrawingclass;
        ShapeFactory shapefactory;
        public Commandparser() { }
        public Commandparser(drawingclass a)
        {
            shapefactory = new ShapeFactory();
            this.Mydrawingclass = a;
        
        }
        public int EvaluateExpression(string expression)
        {
            DataTable datatable = new DataTable();
            expression = ChangeVariables(expression);
            return Convert.ToInt32(datatable.Compute(expression, ""));
        }

        public int Evaluateinputs(string inputs)
        {
            if (int.TryParse(inputs, out int output))
            {
                return output;
            }
            if (VariablesToValueDictionary.ContainsKey(inputs))
            {
                return Convert.ToInt32(VariablesToValueDictionary[inputs]);
            }
            return 0;

        }
        public bool ExecuteLine(string command)
        {
            //creates char array called mychars and splits by a comma, space and backslash
            char[] mychars = { ',', ' ', '/' };
            String[] input = command.Split(mychars);
            int a;

            /**
             * creating switch cases for different functions called for example "moveto" or "drawto" etc
             * this will go into string variable and convert all inputs into lowercase
             */
            switch (input[0].ToLower())
            {
                case "moveto":
                    //see if the correct arguments been entered else throw error
                    if (input.Length > 2 || input.Length < 2)
                    {
                        Mydrawingclass.MoveTo(Evaluateinputs(input[1]), Evaluateinputs(input[2]));
                    }
                    else
                    {
                        MessageBox.Show("Invalid number of operands", "Moveto Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    break;

                case "drawto":
                    //see if the correct arguments been entered else throw error
                    if (input.Length > 2 || input.Length < 2)
                    {
                        Mydrawingclass.DrawLine(Evaluateinputs(input[1]), Evaluateinputs(input[2]));
                    }
                    else
                    {
                        MessageBox.Show("Invalid number of operands", "Drawto Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    break;

                case "circle":
                    //if incorrect arguments are inputted output error
                    if (input.Length > 2 || input.Length < 2)
                    {
                        MessageBox.Show("Invalid parameters passed \n Please enter an integer", "Circle Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    else if (int.TryParse(Evaluateinputs(input[1]).ToString(), out a) == false)
                    {
                        MessageBox.Show("Radius must be an integer", "Circle Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    Mydrawingclass.DrawCircle(Evaluateinputs(input[1]));
                    break;

                case "triangle":
                    //if arguments are correct, output a triangle
                    if (input.Length == 2)
                    {
                        Mydrawingclass.DrawTriangle(Evaluateinputs(input[1]));
                    }
                    else
                    {
                        MessageBox.Show("Invalid parameters passed", "Triangle Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case "rect":
                    //if incorrect arguments are entered show a messagebox or else draw a rectangle
                    if (input.Length == 3 || input.Length == 3)
                    {
                        Mydrawingclass.DrawRectangle(Evaluateinputs(input[1]), Evaluateinputs(input[2]));
                    }
                    else
                    {
                        MessageBox.Show("Invalid parameters passed \n Please enter two integers as 'x,x' ", "Rectangle Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case "fill":
                    //if fill is true then fills shape
                    if (int.Parse(input[1]) == 1)
                    {
                        Mydrawingclass.setFill(true);
                    }
                    if (int.Parse(input[1]) == 0)
                    {
                        Mydrawingclass.setFill(false);
                    }
                    break;

                case "clear":
                    //clear the outputbitmap
                    if (input.Length == 1)
                    {
                        Mydrawingclass.Clear();
                    }
                    break;

                case "reset":
                    //move the pen to position 0,0
                    if (input.Length == 1)
                    {
                        Mydrawingclass.Reset();
                    }
                    break;

                case "pen":
                    //change the pen colour to what the user inputs
                    Mydrawingclass.SetPenColor(input[1]);
                    break;
                default:
                    if (input.Length > 2 && input[1] == "=")
                    {
                        if (input.Length < 3)
                        {
                            throw new Exception();
                        }
                        string expression = command.Substring(command.IndexOf('=') + 1);
                        int value = EvaluateExpression(expression);
                        VariablesToValueDictionary[input[0]] = value.ToString();
                    }
                    else
                    {
                        throw new Exception("Invalid operation");
                    }
                    break;
            }
            return true;
        }

        //allows the user to input a letter in as the variable
        private string ChangeVariables(string commandline)
        {
            string ChangedLine = commandline;

            foreach (string key in VariablesToValueDictionary.Keys)
            {
                ChangedLine = Regex.Replace(ChangedLine, @"\b(" + key + @")\b", VariablesToValueDictionary[key].ToString());
            }
            return ChangedLine;
        }

        //to run the richtextbox
        public bool ExecuteScript(string script)
        {
            String[] input1 = script.Split('\n');
            String[] input1words = null;
            string changedline = null;

            for (int i = 0; i < input1.Length; i++)
            {
                changedline = input1[i];
                input1words = input1[i].Split(" ");
                if (changedline.Contains("if") && !changedline.Equals("endif"))
                { 
                    int IfCommandI = 0;
                    int EndIfI = 0;
                    string IfOperator = "";
                    string Ifnumber1 = "";
                    string Ifnumber2 = "";
                    //runs through the loop till the end
                    foreach (string ifcommand in input1)
                    {
                        if (ifcommand.Contains("if") && !ifcommand.Equals("endif"))
                        {
                            IfCommandI = Array.IndexOf(input1, ifcommand);
                            IfOperator = input1[IfCommandI].Split(" ")[2];
                            //store the number becauuse the line will be blanked ouut
                            Ifnumber1 = ChangeVariables(input1[IfCommandI].Split(" ")[1]);
                            Ifnumber2 = ChangeVariables(input1[IfCommandI].Split(" ")[3]);

                            //changing the line
                            input1[Array.IndexOf(input1, ifcommand)] = "";
                        }

                        if (ifcommand.Equals("endif"))
                        {
                            EndIfI = Array.IndexOf(input1, ifcommand);
                            input1[Array.IndexOf(input1, ifcommand)] = "";
                            break;
                        }
                    }
                    if (EndIfI == 0)
                    {
                        MessageBox.Show("Invalid Command. Incorrect number of parameters passed!", "If Statement Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        EndIfI = input1.Length;
                    }

                    //keeps commands in the list
                    List<string> IfCommandsLoop = new List<string>();
                    //copy the commands inbetween to the list
                    for (int n = IfCommandI + 1; n < EndIfI; n++)
                    {
                        IfCommandsLoop.Add(input1[n]);
                        //blank out commands so they dont get executed
                        input1[Array.IndexOf(input1, input1[n])] = "";
                    }
                    foreach (string ifcommand in IfCommandsLoop)
                    {
                        switch (IfOperator)
                        {
                            case "==":
                                if (Ifnumber1.Equals(Ifnumber2))
                                {
                                    ExecuteLine(ifcommand);
                                }
                                break;
                            case "<=":
                                if (int.Parse(Ifnumber1) <= int.Parse(Ifnumber2))
                                {
                                    ExecuteLine(ifcommand);
                                }
                                break;
                            case ">=":
                                if (int.Parse(Ifnumber1) >= int.Parse(Ifnumber2))
                                {
                                    ExecuteLine(ifcommand);
                                }
                                break;
                            case ">":
                                if (int.Parse(Ifnumber1) > int.Parse(Ifnumber2))
                                {
                                    ExecuteLine(ifcommand);
                                }
                                break;
                            case "<":
                                if (int.Parse(Ifnumber1) < int.Parse(Ifnumber2))
                                {
                                    ExecuteLine(ifcommand);
                                }
                                break;
                            default:
                                MessageBox.Show("Invalid Operator. If operation unknown.", "If Statement Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                        }
                    }
                }

                if (changedline.Contains("while") && !changedline.Equals("endwhile"))
                {
                    int WhileCommandI = 0;
                    int EndWhileCommandI = 0;
                    int LoopTimes = 0;
                    //runs through the loop till the end
                    foreach (string whilecommand in input1)
                    {
                        if (whilecommand.Contains("while") && !whilecommand.Equals("endwhile"))
                        {
                            WhileCommandI = Array.IndexOf(input1, whilecommand);
                            LoopTimes = int.Parse(ChangeVariables(input1[WhileCommandI].Split(" ")[1]));
                            //changing the line
                            input1[Array.IndexOf(input1, whilecommand)] = "";
                        }

                        if (whilecommand.Equals("endwhile"))
                        {
                            EndWhileCommandI = Array.IndexOf(input1, whilecommand);
                            input1[Array.IndexOf(input1, whilecommand)] = "";
                            break;
                        }
                    }
                    if (EndWhileCommandI == 0)
                    {
                        MessageBox.Show("Invalid Command. Incorrect number of parameters passed!", "While Loop Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        EndWhileCommandI = input1.Length;
                    }

                    //keeps command in the loop
                    List<string> WhileCommandsLoop = new List<string>();
                    //copy the commands inbetween to the list
                    for (int n = WhileCommandI + 1; n < EndWhileCommandI; n++)
                    {
                        WhileCommandsLoop.Add(input1[n]);
                        input1[Array.IndexOf(input1, input1[n])] = "";
                    }
                    //pass the list through in order to be looped
                    for (int x = 0; x < LoopTimes; x++)
                    {
                        foreach (string command in WhileCommandsLoop)
                        {
                            if (!command.Equals(""))
                            {
                                if (!command.Contains("="))
                                {
                                    string changedCommand = ChangeVariables(command);
                                    ExecuteLine(changedCommand);
                                }
                                else
                                {
                                    ExecuteLine(command);
                                }
                            }
                        }
                    }
                }
                if (!input1[i].Equals(""))
                {
                    ExecuteLine(changedline);
                }
            }
            return true;
        }
    }
}