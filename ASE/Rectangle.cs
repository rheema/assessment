﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ASE
{
    public class Rectangle : Shape
    {
        protected int width, height;
        public Rectangle() : base() { }
        public override void Filled(Graphics gp)
        {
            if (fill)
            { 
                gp.FillRectangle(Brush, xPos, yPos, xPos + width, yPos + height);
            }
            else
            {
                gp.DrawRectangle(Pen, xPos, yPos, xPos + width, yPos + height);
            }
        }
        public override void Set(Color colour, int x, int y, params string[] param)
        {
            base.Set(colour, x, y, param);
            width = int.Parse(param[0]);
            height = int.Parse(param[1]);
        }
    }

}

