﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ASE
{
    class Triangle : Shape
    {
        public Triangle() : base() { }
        protected int size;
        public Triangle(Color colour1, Color colour2, int size, int xPos, int yPos)
            : base(colour1, colour2, xPos, yPos)
        {
            this.size = size;
        }
        public override void Set(Color colour, int x, int y, params string[] param)
        {
            base.Set(colour, x, y, param);
            size = int.Parse(param[0]);
        }
        public override void Filled(Graphics gp)
        {
            PointF point1 = new PointF(xPos, yPos);
            PointF point2 = new PointF(xPos + size, yPos);
            PointF point3 = new PointF(xPos, yPos + size);
            PointF[] curvePoints =
                {
                 point1,
                 point2,
                 point3,
                };
            if (!fill)
            {
                gp.DrawPolygon(Pen, curvePoints);
            }
            else
            {
                gp.FillPolygon(Brush, curvePoints);
            }
        }

    }
}
