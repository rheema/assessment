﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ASE
{
    public class Circle : Shape
    {
        protected int radius;

        //takes the arguments in the circle and puts it in the base
        public Circle() :base(){ }

        public Circle(Color colour1, Color colour2, int radius, int xPos, int yPos) 
            : base (colour1, colour2, xPos, yPos)
        {
            this.radius = radius;
        }
        public override void Set(Color colour, int x, int y, params string[] param)
        {
            base.Set(colour, x, y, param);
            radius = int.Parse(param[0]);
        }

        public override void Filled(Graphics gp)
        {
            if (!fill)
            {
                gp.DrawEllipse(Pen, xPos, yPos, radius, radius);
            }
            else
            {
                gp.FillEllipse(Brush, xPos, yPos, radius, radius);
            }
        }
    }
}
