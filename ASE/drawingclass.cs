﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE
{
    /// <summary>
    /// drawingclass created to hold information that is outputted on the form
    /// </summary>
    public class drawingclass : Commandparser
    {
        //creating the instance for the graphic and pen x,y position
        Graphics gp;
        Pen Pen;
        Brush Brush;
        protected List<Shape> Shapes;
        protected bool fill;
        ShapeFactory sf = new ShapeFactory();
       
        public int xPos { get; set; }
        public int yPos { get; set; }

        /// <summary>
        /// the constructor initialises the graphicoutput to use a black pen and brush
        /// </summary>
        /// <param name="gp">Graphics context of area to draw on</param>
        public drawingclass(Graphics gp)
        {
            //the gp data instance
            this.gp = gp;
            xPos = yPos = 0;
            Pen = new Pen(Color.Black, 2); //pen is for the border of shape
            Brush = new SolidBrush(Color.Black); //brush is intside colour
            Shapes = new List<Shape>();
        }

        public void setFill(bool fill)
        {
            this.fill = fill;
        }

        public void InsertShape(Shape shape)
        {
            Shapes.Add(shape);
        }

        /// <summary>
        /// created a method to allow end user to change colours
        /// </summary>
        /// <param name="pencolour">name of the string</param>
        public void SetPenColor(string pencolour)
        {
            Pen = new Pen(Color.FromName(pencolour));
            Brush = new SolidBrush(Color.FromName(pencolour));
        }

        /// <summary>
        /// to move the position of the pen
        /// </summary>
        /// <param name="toX">the position of the x- axis</param>
        /// <param name="toY">the position of y-axis</param>
        public void MoveTo(int toX, int toY)
        {
            //to move the position of pen
            xPos = toX;
            yPos = toY;
        }

        /// <summary>
        /// to draw lines to coordinates provided by user
        /// </summary>
        /// <param name="toX">position to draw to</param>
        /// <param name="toY">postition to draw tp</param>
        public void DrawLine(int toX, int toY)
        {
            gp.DrawLine(Pen, xPos, yPos, toX, toY);
            this.MoveTo(toX, toY);
        }

        /// <summary>
        /// to draw a rectangle with width and height provided by user
        /// </summary>
        /// <param name="width">measurement of the side</param>
        /// <param name="height">measurement of side</param>
        public void DrawRectangle(int width, int height)
        {
            Rectangle rectangle = (Rectangle)sf.getShape("rectangle");
            rectangle.Set(this.Pen.Color, xPos, yPos, width.ToString(), height.ToString());

            if (!fill)
            {
                rectangle.Filled(gp);
            }
            else
            {
                rectangle.SetFill(true);
                rectangle.Filled(gp);
            }
        }

        /// <summary>
        /// to fill in a rectangle
        /// </summary>
        /// <param name="width">measurement of side</param>
        /// <param name="height">measurement of side</param>
        public void FillRectangle(int width, int height)
        {
            gp.FillRectangle(Brush, xPos, yPos, xPos + width, yPos + height);
        }

        /// <summary>
        /// to draw a circle
        /// </summary>
        /// <param name="radius">measurement from centre of circle to circumference</param>
        public void DrawCircle(int radius)
        {
            Circle circle = (Circle)sf.getShape("circle");
            circle.Set(this.Pen.Color, xPos, yPos, radius.ToString());

            if (!fill)
            {
                circle.Filled(gp);
            }
            else
            {
                circle.SetFill(true);
                circle.Filled(gp);
            }
        }

        /// <summary>
        /// to draw a triangle
        /// </summary>
        /// <param name="size">measurement for triangle</param>
        public void DrawTriangle(int size)
        {
            Triangle Triangle = (Triangle)sf.getShape("triangle");
            Triangle.Set(this.Pen.Color, xPos, yPos, size.ToString());

            if (!fill)
            {
                Triangle.Filled(gp);
            }
            else
            {
                Triangle.SetFill(true);
                Triangle.Filled(gp);
            }
        }

        /// <summary>
        /// to fill the triangle
        /// </summary>
        /// <param name="size">measurement for triangle</param>
        public void FillTriangle(int size)
        {
            PointF point1 = new PointF(xPos, yPos);
            PointF point2 = new PointF(xPos + size, yPos);
            PointF point3 = new PointF(xPos, yPos + size);
            PointF[] curvePoints =
                     {
                 point1,
                 point2,
                 point3,
             };

            // Draw polygon curve to screen.
            gp.FillPolygon(Brush, curvePoints);
        }

        /// <summary>
        /// to clear the shapes or lines drawn
        /// </summary>
        public void Clear()
        {
            gp.Clear(Color.Gray);
        }

        /// <summary>
        /// to reset the pen to 0,0 
        /// </summary>
        public void Reset()
        {
            xPos = 0;
            yPos = 0;
        }
    }
}