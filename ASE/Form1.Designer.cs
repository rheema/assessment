﻿
namespace ASE
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.programscript = new System.Windows.Forms.RichTextBox();
            this.graphicoutput = new System.Windows.Forms.PictureBox();
            this.runprogrambtn = new System.Windows.Forms.Button();
            this.commandLine = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportBmpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.graphicoutput)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // programscript
            // 
            this.programscript.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.programscript.Location = new System.Drawing.Point(12, 27);
            this.programscript.Name = "programscript";
            this.programscript.Size = new System.Drawing.Size(336, 240);
            this.programscript.TabIndex = 0;
            this.programscript.Text = "";
            // 
            // graphicoutput
            // 
            this.graphicoutput.BackColor = System.Drawing.Color.Gray;
            this.graphicoutput.Location = new System.Drawing.Point(377, 27);
            this.graphicoutput.Name = "graphicoutput";
            this.graphicoutput.Size = new System.Drawing.Size(590, 418);
            this.graphicoutput.TabIndex = 1;
            this.graphicoutput.TabStop = false;
            this.graphicoutput.Paint += new System.Windows.Forms.PaintEventHandler(this.Graphicoutput_Paint);
            // 
            // runprogrambtn
            // 
            this.runprogrambtn.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.runprogrambtn.Location = new System.Drawing.Point(102, 273);
            this.runprogrambtn.Name = "runprogrambtn";
            this.runprogrambtn.Size = new System.Drawing.Size(141, 41);
            this.runprogrambtn.TabIndex = 2;
            this.runprogrambtn.Text = "Run Program";
            this.runprogrambtn.UseVisualStyleBackColor = true;
            this.runprogrambtn.Click += new System.EventHandler(this.Runprgbtn_Click);
            // 
            // commandLine
            // 
            this.commandLine.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.commandLine.Location = new System.Drawing.Point(12, 320);
            this.commandLine.Name = "commandLine";
            this.commandLine.Size = new System.Drawing.Size(356, 29);
            this.commandLine.TabIndex = 3;
            this.commandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CommandLine_KeyDown);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(979, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.exportBmpToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.LoadToolStripMenuItem_Click);
            // 
            // exportBmpToolStripMenuItem
            // 
            this.exportBmpToolStripMenuItem.Name = "exportBmpToolStripMenuItem";
            this.exportBmpToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exportBmpToolStripMenuItem.Text = "Export Bmp";
            this.exportBmpToolStripMenuItem.Click += new System.EventHandler(this.exportBmpToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 457);
            this.Controls.Add(this.commandLine);
            this.Controls.Add(this.runprogrambtn);
            this.Controls.Add(this.graphicoutput);
            this.Controls.Add(this.programscript);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.graphicoutput)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox programscript;
        private System.Windows.Forms.PictureBox graphicoutput;
        private System.Windows.Forms.Button runprogrambtn;
        private System.Windows.Forms.TextBox commandLine;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportBmpToolStripMenuItem;
    }
}

