﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ASE
{
    class ShapeFactory
    {
        protected Color colour1, colour2;
        protected int x, y;
        protected int radius;
        protected int width, height;
        protected int size;

        public ShapeFactory()
        {
            colour1 = Color.Red;
            colour2 = Color.Green;
            //Pen Pen = new Pen(colour1);
            //Brush Brush = new SolidBrush(colour2);
        }

        public Shape getShape(string shape)
        {
            switch (shape)
            {
                case "circle":
                    return new Circle();
                case "rectangle":
                    return new Rectangle();
                case "triangle":
                    return new Triangle();
                default:
                    throw new NotImplementedException();
            }
        }

        //public Circle DrawCircle(int Radius)
        //{
        //    Circle circleshape = new Circle();
        //    return circleshape;
        //}
        //public Circle FillEllipse(int Radius)
        //{
        //    //gp.FillEllipse(Brush, xPos, yPos, radius, radius);
        //    Circle fillcircle = new Circle(colour1, colour2, Radius, x, y);
        //    radius = Radius;
        //    return fillcircle;
        //}

        //public Rectangle DrawRectangle(int Width, int Height)
        //{
        //    //Rectangle rectshape = new Rectangle(colour1, colour2, Width, Height, x, y);
        //    //width = Width;
        //    //height = Height;
        //    //return rectshape;
        //}
        //public Rectangle FillRectangle(int Width, int Height)
        //{
        //    //Rectangle rectshape = new Rectangle(colour1, colour2, Width, Height, x, y);
        //    //width = Width;
        //    //height = Height;
        //    //return rectshape;
        //}
        //public Triangle DrawTriangle(int size)
        //{
        //    Triangle trishape = new Triangle(colour1, colour2, size, x, y);
        //    this.size = size;
        //    return trishape;
        //}
        //public Triangle FillTriangle(int size)
        //{
        //    Triangle trishape = new Triangle(colour1, colour2, size, x, y);
        //    this.size = size;
        //    return trishape;
        //}


    }

}
