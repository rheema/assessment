﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ASE
{
    //entry point to the program
    public partial class Form1 : Form
    {
        int BitmapWidth;
        int BitmapHeight;
        public Bitmap OutputBitmap;
        public drawingclass Mydrawingclass;
        public Commandparser cp;
        public Form1()
        {
            InitializeComponent();
            //class that will handle the drawingn
            BitmapWidth = 640;
            BitmapHeight = 480;
            OutputBitmap = new Bitmap(BitmapWidth, BitmapHeight);
            Mydrawingclass = new drawingclass(Graphics.FromImage(OutputBitmap));
            cp = new Commandparser(Mydrawingclass);
        }

        //code for the commandLine
        private void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!commandLine.Text.Equals(""))
                {
                    if (commandLine.Text.Equals("run") && !programscript.Text.Equals(""))
                    {
                        cp.ExecuteScript(programscript.Text);
                        commandLine.Text = "";
                        Refresh();
                        return;
                    }
                    else
                    {
                        if(!commandLine.Text.Equals("run"))
                            cp.ExecuteLine(commandLine.Text);
                    }
                    commandLine.Text = "";
                }
                Refresh();
            }
        }
       //handles what happens when the run program button is clicked
        private void Runprgbtn_Click(object sender, EventArgs e)
        {
            if(!programscript.Text.Equals(""))
                cp.ExecuteScript(programscript.Text);

            Refresh();
        }
        
        //allows the script to be saved as a textfile
        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog
            {
                //filters so user can see txt files 
                Filter = "txt files (*.txt)|*.txt"
            };
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                using Stream s = File.Open(saveFile.FileName, FileMode.CreateNew);
                using StreamWriter sw = new StreamWriter(s);
                sw.Write(programscript.Text);
            }
        }
        //allows the script to be opened in in the program script
        private void LoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using OpenFileDialog Openfile = new OpenFileDialog();
          
            Openfile.Filter = "txt files (*.txt)|*.txt";
            if (Openfile.ShowDialog() == DialogResult.OK)
            {
                string filePath = Openfile.FileName;
                var fileStream = Openfile.OpenFile();

                using StreamReader reader = new StreamReader(fileStream);
                // get the contents of the file and put it in the script area
                programscript.Text = reader.ReadToEnd();
                reader.Close();
            }
        }

        //will display the output
        private void Graphicoutput_Paint(object sender, PaintEventArgs e)
        {
            Graphics gp = e.Graphics;
            gp.DrawImageUnscaled(OutputBitmap, 0, 0);
        }

        private void exportBmpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog
            {
                Filter = "Bitmap files (*.bmp)|*.bmp|All files(*.*)|*.*"
            };
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                string filePath = Path.GetFullPath(saveFile.FileName);
                OutputBitmap.Save(filePath);
            }
        }
    }
}