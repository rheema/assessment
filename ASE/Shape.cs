﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ASE
{
    /// <summary>
    /// An abstract class for shapes used
    /// </summary>
   public abstract class Shape
    {
        protected Pen Pen;
        protected Brush Brush;
        protected int xPos, yPos;
        protected bool fill;
        protected bool flashing;
        protected bool changecolour;
        protected Color colour1, colour2;
        //changecolour = true should be colour1 and if false colour2

        protected Shape() {
            this.colour1 = Color.Black;
            this.colour2 = Color.Black;
            Pen = new Pen(colour1);
            fill = false;
            Brush = new SolidBrush(colour1);
            this.xPos = 0;
            this.yPos = 0;
            changecolour = true;
        }

        public Shape(Color colour1, Color colour2, int xPos, int yPos)
        {
            this.colour1 = colour1;
            flashing = false;
            this.fill = false;
            Pen = new Pen(colour1);
            Brush = new SolidBrush(colour1);
            this.xPos = xPos;
            this.yPos = yPos;
            changecolour = true;
        }
        
       
        //destructor called when a class is destroyed

        public virtual void Set(Color colour, int x, int y, params string[] param)
        {
            this.colour1 = colour;
            Pen = new Pen(colour);
            Brush = new SolidBrush(colour);
            this.xPos = x;
            this.yPos = y;
        }

        public void SetFill(bool fill)
        {
            this.fill = fill;
        }
        public void SetFlashing(Color colour2)
        {
            this.colour2 = colour2;
            flashing = true;
        }
        public void SwapColour()
        {
            if (changecolour)
            {
                Pen.Color = colour2;
                Brush = new SolidBrush(colour2);
                changecolour = false;
            }
            else
            {
                Pen.Color = colour1;
                Brush = new SolidBrush(colour1);
                changecolour = true;
            }
        }
        
        public abstract void Filled(Graphics gp);

    }
}