using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using ASE;

namespace UnitTest
{
    [TestClass]
    public class ASEUnitTest
    {
        [TestMethod]
        //test to check if the drawto function is working
        public void TestDrawTo()
        {   
            Form1 form1 = new Form1();
            drawingclass drawingclass = form1.Mydrawingclass;
            Commandparser commandparser = form1.cp;
            Assert.IsTrue(commandparser.ExecuteLine("drawto 100 100"));
        }

        [TestMethod]
        //test to see if the moveto function works by moving to the coordinates
        public void TestMoveTo()
        {
            Form1 form1 = new Form1();
            drawingclass drawingclass = form1.Mydrawingclass;
            drawingclass.MoveTo(0, 0);
            Assert.AreEqual(drawingclass.xPos, 0);
            Assert.AreEqual(drawingclass.yPos, 0);
        }

        [TestMethod]
        //test to see if the draw circle function works with the radius
        public void TestDrawCircle()
        {
            Form1 form1 = new Form1();
            drawingclass drawingclass = form1.Mydrawingclass;
            Commandparser commandparser = form1.cp;
            Assert.IsTrue(commandparser.ExecuteLine("circle 50"));
        }
        
        [TestMethod]
        //test to see if the rectangle will output with width and height
        public void TestDrawRectangle()
        {
            Form1 form1 = new Form1();
            drawingclass drawingclass = form1.Mydrawingclass;
            Commandparser commandparser = form1.cp;
            drawingclass.DrawRectangle(10, 100); 
            Assert.IsTrue(commandparser.ExecuteLine("rect 10 100"));
        }

        [TestMethod]
        //test to see if the draw triangle function works with the radius
        public void TestDrawTriangle()
        {
            Form1 form1 = new Form1();
            drawingclass drawingclass = form1.Mydrawingclass;
            Commandparser commandparser = form1.cp;
            Assert.IsTrue(commandparser.ExecuteLine("triangle 100"));
        }

        [TestMethod]
        //test to see if fill is true
        public void Fill()
        {
            Form1 form1 = new Form1();
            Commandparser commandparser = form1.cp;
            Assert.IsTrue(commandparser.ExecuteLine("fill 1"));
        }

        [TestMethod]
        //test to see if the variable input works
        public void TestChangedVariables()
        {
            Form1 form1 = new Form1();
            drawingclass drawingclass = form1.Mydrawingclass;
            Commandparser commandparser = form1.cp;
            Assert.IsTrue(commandparser.ExecuteScript("x = 100\ncircle x"));

        }

        //[TestMethod]
        ////Part two testing
        //public void TestForLoop()
        //{
        //    string[] looptext = new string[] { "moveto 50 50", "drawto 20,220" };
        //    int countreturn = 0;
        //    Form1 form1 = new Form1();
        //    drawingclass drawingclass = form1.Mydrawingclass;
        //    Commandparser commandparser = form1.cp;

        //    //countreturn = drawingclass.ForLoop(looptext, looptext.Length);
        //    Assert.AreEqual(2, countreturn);
        //}
    }
}